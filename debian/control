Source: libaccounts-qt
Section: libs
Priority: optional
Maintainer: Debian/Kubuntu Qt/KDE Maintainers <debian-qt-kde@lists.debian.org>
Uploaders: Maximiliano Curia <maxy@debian.org>, Diane Trout <diane@debian.org>
Build-Depends: debhelper (>= 9),
               libaccounts-glib-dev (>= 1.23),
               libglib2.0-dev,
               pkg-config,
               qt5-qmake,
               qtbase5-dev,
               qtchooser
Build-Depends-Indep: doxygen, graphviz, qttools5-dev-tools
Standards-Version: 4.1.2
Homepage: https://gitlab.com/accounts-sso/libaccounts-qt
Vcs-Browser: https://anonscm.debian.org/git/pkg-kde/kde-extras/libaccounts-qt.git
Vcs-Git: https://anonscm.debian.org/git/pkg-kde/kde-extras/libaccounts-qt.git

Package: libaccounts-qt5-dev
Section: libdevel
Architecture: any
Depends: libaccounts-qt5-1 (= ${binary:Version}),
         ${misc:Depends},
         ${shlibs:Depends}
Description: Accounts database access Qt version - Qt5 development files
 Accounts and SSO (Single Sign-On) framework for Linux and POSIX based
 platforms.
 .
 This package contains files the development files of the accounts database
 access Qt5 API.

Package: libaccounts-qt5-1
Architecture: any
Pre-Depends: ${misc:Pre-Depends}
Depends: ${misc:Depends}, ${shlibs:Depends}
Description: Accounts database access Qt version - Qt5 shared library
 Accounts and SSO (Single Sign-On) framework for Linux and POSIX based
 platforms.
 .
 This package contains shared libraries to be used by Qt5 applications.

Package: libaccounts-qt-doc
Section: doc
Architecture: all
Depends: libjs-jquery,
         ${misc:Depends}, ${shlibs:Depends}
Description: Accounts database access Qt version - documentation
 Accounts and SSO (Single Sign-On) framework for Linux and POSIX based
 platforms.
 .
 This package contains developer documentation.
